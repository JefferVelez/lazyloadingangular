import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

enum LazyLoadingType {
  Normal = 1,
  Detail = 2,
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  form: FormGroup;

  LazyLoadingType = LazyLoadingType;

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.form = fb.group({
      id: [null, Validators.required],
      base64: [null, Validators.required],
      type: [null],
    });
  }

  fileSelected(e) {
    this.base64FileInput(e, (base64) => {
      this.form.controls.base64.setValue(base64);
    });
  }

  submit(type: LazyLoadingType) {
    if (!this.form.valid) return;

    this.form.controls.type.setValue(type);

    this.http
      .post(environment.apiUrl + 'main/rides', this.form.value)
      .subscribe(
        (res) => {
          this.download(
            <string>res,
            'text/plain',
            'txt',
            'lazy_loading_example_output'
          );
        },
        (err) => {
          alert(err.message);
        }
      );
  }

  private base64FileInput(e, onLoad: (base64: string) => void) {
    if (!e.target.files.length) return;

    for (const file of e.target.files) {
      this.base64Blob(file, onLoad);
    }
  }

  private base64Blob(file: File, onLoad: (base64: string) => void) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      onLoad.call(this, reader.result);
    });
    reader.readAsDataURL(file);
  }

  private download(
    encodedFile: string,
    mime: string,
    ext: string,
    fileName: string
  ) {
    const decodedFileData = atob(encodedFile);
    const byteNumbers = new Array(decodedFileData.length);
    for (let i = 0; i < decodedFileData.length; i++) {
      byteNumbers[i] = decodedFileData.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: mime });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.href = url;
    a.download = fileName + '.' + ext;
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }
}
